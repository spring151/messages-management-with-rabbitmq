package com.example.mobitelspringrabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com")
public class MobitelSpringRabbitmqApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobitelSpringRabbitmqApplication.class, args);
	}

}
